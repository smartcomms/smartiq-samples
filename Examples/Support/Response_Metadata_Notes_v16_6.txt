Feature: Response Metadata

Project Version: 16.6

This example project shows you how you can use a Response Metadata Field within a text field. 


Note:  The metadata response field created in Manage> Custom Fields was "Metadata Example". This was placed underneath the Text field.  

The text field can be used within a guided form and then loaded within a Datasource to display all important information or fields 
that are not within our System, becoming a valueable asset to your Design Implementations. 