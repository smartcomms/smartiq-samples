KH article: https://knowledgehub.smartcommunications.com/docs/email-approvalworkflow-choice

This sample project and Email approval feature has a special usecase as in to complete the workflow step 
even without interacting with SmartIQ just by either approving or rejecting the request from the email itself.

This is generally only to be used in the case where you would not even review the source and contents and trust the Source itself 
like in case of a Manager Task to an Admin where the Admin would starightaway approve it.

Here in this project, the Hidden Approval Status choices Yes and No are sent over email with 
not much details about the Form or Summary(a bit of HTML and Design Code required)and Upon Selection 
completes the workflow from the email then and there without in a way interacting or opening SmarIQ.

Note: To make sure this feature is working Please check if your admin has a valid email address in Manage and 
you have also Configured your SMTP settings, try clicking on the approve button in email and you are done with the approval.