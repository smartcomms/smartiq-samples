Send Email action: https://knowledgehub.smartcommunications.com/docs/send-email-action
SMTP configuration: https://knowledgehub.smartcommunications.com/docs/configuring-smtp

Project shows how to use send email action while attachind a document in the email containing placeholders.