Rest Data Connection - Open Weather - No Credentials v16.3.2

Project Description:

Run the project in produce, form should display current weather details by city Canberra, AU

https://knowledgehub.smartcommunications.com/docs/rest-1#get-method


Notes:
For the project to work the following are required - An account on free plan with Open Weather, API Key, and a Rest Data Connection to be configured.


Import Sample Project:

1. Download the project from Bit Bucket
2. Go to Manage > Projects > Import
3. Choose Project File (.ixpkg) > click Upload > click Import > Publish > Save.


Create Sandbox with Open Weather and get API Key:

1. Go to https://openweathermap.org/price
2. Click on Get API Key under Free plan.
3. Sign Up with an email address and then click on the Verification Email in your inbox.
4. You will recieve an email with the API Key.


Configure REST Data Connection:

1. Go to Manage > Data Connections > New Data Connection and select Connection Type as REST.
2. Put baseurl=http://api.openweathermap.org; in the Connection Attributes
3. Choose No credentials required and Save
4. Create a New Data Object with type method and definition /data/2.5/weather?q=Canberra,au&appid=b0192f749476ae227c9fb2872113d36e&units=metric
5. Add all filter fields > Save.



