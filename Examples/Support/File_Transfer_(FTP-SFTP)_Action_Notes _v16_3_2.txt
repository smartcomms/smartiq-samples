File Transfer (FTP - SFTP) Action v16.3.2

Project Description:

Run the project in produce, you can click the links to demo the Direct Link and Portal method.

https://knowledgehub.smartcommunications.com/docs/file-transfer-ftpsftp-action


Notes:
For the project to work a valid ftp or sftp server and credentials are required.


Import Sample Project:

1. Download the project from Bit Bucket
2. Go to Manage > Projects > Import
3. Choose Project File (.ixpkg) > click Upload > click Import > Publish > Save.





