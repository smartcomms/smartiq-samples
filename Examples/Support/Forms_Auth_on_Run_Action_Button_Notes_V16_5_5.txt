Forms Authentication: https://knowledgehub.smartcommunications.com/reference/post_login-forms-2
Run Action Button: https://knowledgehub.smartcommunications.com/docs/run-action-button-question-type


Project shows how to configure forms authentication on a Run Action Button. The response is an authentication token.
Please check the run action button inputs for more details.