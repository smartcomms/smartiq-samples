KH article: https://knowledgehub.smartcommunications.com/docs/scheduler


This project simply sends an email to the mentioned To address in the Form Design when run in Produce

Assumptions(See screenshots in the same folder):
1. Making sure you have already configured your SMTP Settings
2. Scheduler is Installed, configured and Running
3. You have access to Manage Portal and the project folder where you would run this project
4. You may require access to the DB to check the NextRunDate and time 
5. The current project is imported and Pubhlished


To run this project please make sure you enter the new email address in the Address To: 
in Send email action under Finish tab in Design of this project and click save.

This project would just simply send and email with Test Email as the subject and Body with Body: Test Email

The Real use of project comes in handy to explain the use of scheduler in SmartIQ

On the manage Portal for the current project go to Pubhlished Folders and select it, 
upon selection click on the Schedule button on the top(check screenshot for the same Definition) 
and set the scheduler as shown in the picture

After setting this up please go to Management and click on Definitions,
Here you would be able to see the Scheduled job 

When the scheduler runs this as defined in here(10 times) you could see it running every minute and the associated status with it

Also on the output side for the same, we could see 10 email coming in with our designed email in a span of 1 minute each till the time the job is completed

Note: This project is being setup just to show the Scheduler's use in the simplest way 





