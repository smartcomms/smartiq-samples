Notes - Web Service Data Connection - Remote Server - Windows Authentication v16.3.2

Project Description:

Run the project in produce, Sum of two numbers will be calculated using the webservice hosted on a remote machine.
The web service data connection in SmartIQ is using windows user credentials to authenticate.
The web service hosted on Remote IIS Server has Anonymous Authentication disabled and Windows Authentication Enabled.

For more info refer https://knowledgehub.smartcommunications.com/docs/web-service-soap


Notes:
For the project to work the following are required - A remote web server with IIS, install the web service with windows Authentication enabled, and a SmartIQ data connection using a valid Windows username and password to the remote web server using Connect with these credentials option instead of the Windows Authentication option.
The Webserice was built using Visual Studio C#


Import Sample Project:

1. Download the project from Bit Bucket
2. Go to Manage > Projects > Import
3. Choose Project File (.ixpkg) > click Upload > click Import > Publish > Save.


Install Webservice on Remote web server (Below e.g for Support Team Only):

1. Download ArithmeticOperationsWebService.zip from Bit Bucket.
2. Install the Webservice on IIS in its own AppPool and Windows Authentication to be enabled.


Configure Web Service Data Connection:

1. Go to Manage > Data Connections > New Data Connection and select Connection Type as Web Service.
2. Put the WSDL path in the Connection Attributes
3. Choose Connect with these credentials and provide the windows username and password and Save.
4. Create a New Data Object with Add method and all filter fields. > Save.



