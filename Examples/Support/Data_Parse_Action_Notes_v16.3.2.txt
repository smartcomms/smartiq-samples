Data Parse Action Demo


https://knowledgehub.smartcommunications.com/docs/data-parse-action

https://jsonpath.com/
https://codebeautify.org/Xpath-Tester
https://www.freeformatter.com/json-to-xml-converter.html



1. Use the default values provided on form or provide new Json data and xml data for parsing
2. Provide JSON Path and XML Path formats
3. Submit the form
4. Verify the Display Messages show correct results for the parsed data on the finish page