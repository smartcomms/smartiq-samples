Copy Project Results to SQL Action

https://knowledgehub.smartcommunications.com/docs/project-results-to-sql-server-action

For this demo to work, ensure Default SQL Server Connection String set is configured in Manage > Settings > Project Results to SQL


1. Enter the values and submit the form.
2. Verify Project is completed in Management Console
3. Below 5 tables are automatically created on first run.
	Results_Copyprojectresultstosqlactionv16.3.2
	Results_Copyprojectresultstosqlactionv16.3.2_[GUID]
	Results_SchemaColumnMap
	Results_SchemaProjectMap
	Results_SchemaUpdate
4. Add new text fields in design and verify changes to tables in the database.
