﻿## Functions ##

function YNPrompt
{
  param(
     [Parameter()]
     [string]$promptString
  )
  $response = $null
  while ($response -ne "y" -and $response -ne "n")
  {
    $response = Read-Host -Prompt $promptString
    if ($response.Length > 0)
    {
      $response = $response[0].ToLower()
    }
  }
  return $response
}

function PromptForDate
{
  param(
     [Parameter()]
     [string]$DateName
     )
  do {
    $date = $null
    $input = Read-Host -Prompt ("Enter ${DateName}. Format yyyy-MM-dd (e.g. {0}) " -f (Get-Date -Format "yyyy-MM-dd"))

    try {
        $date = Get-Date -Date $input -Format "yyyy-MM-dd" -ErrorAction Stop
        #'{0} is a valid date' -f $start
    }
    catch {
        Write-Host '{0} is an invalid date' -f $input
    }
  }
  until ($date)
  return $date
}

function DeleteWorkflowTask
{
  param(
     [Parameter()]
     [string]$taskId
     )
  $deleteRequest = Invoke-WebRequest `
   -Uri "$Url/workflow/$taskId" `
   -Method 'Delete' `
   -Authentication 'Bearer' -Token $Token 
  if ($deleteRequest.StatusCode -eq 204)
  {
    Write-Host "delete succeeded"
  }
  else 
  {
    Write-Host "delete failed"
  }
}

function GetWorkflowTasks
{
  Invoke-RestMethod `
   -Uri "$Url/workflow" `
   -Method 'Get' `
   -Authentication 'Bearer' -Token $Token
}

function ChooseProject
{ 
  $publishedProjectsResponse = Invoke-RestMethod `
    -Uri "$Url/publishedprojects" `
    -Method 'Get' `
    -Authentication 'Bearer' -Token $Token

  $projectsWithFolder = @()
  foreach ($folder in $publishedProjectsResponse.folders)
  {
    foreach ($publish in $folder.forms)
    {
      $taskCount = $global:operatingArray.Where({ $_.publishId -eq $publish.id }).Count

      if ($taskCount -gt 0)
      {
        $projectsWithFolder += [pscustomobject]@{
          name = $publish.name
          publishId = $publish.id
          folder = $folder.name
          tasks = $taskCount
        }
      }
    }
  }
  
  return ($projectsWithFolder | Sort-Object -Descending tasks | Out-GridView -OutputMode Single)
}

#### Logic ####
Write-Host "SmartIQ workflow purge tool - welcome"

$baseurl = Read-Host -Prompt "Manage URL"
$username = Read-Host -Prompt "Username"
$securepass = Read-Host -Prompt "Password" -AsSecureString
$password = ConvertFrom-SecureString -SecureString $securepass -AsPlainText

$Url = $baseurl + "/api/v1/admin"
$LoginBody = @{
    username = $username
    password = $password
}

Write-Host "Logging in to $Url/login/forms ...."
$Auth = Invoke-RestMethod `
 -Uri "$Url/login/forms" `
 -Body ($LoginBody|ConvertTo-Json) -Method 'Post' `
 -ContentType 'application/json'

$Token = ConvertTo-SecureString $Auth.AuthorizationToken -AsPlainText

Write-Host "Login complete"

#get workflow tasks
Write-Host "Fetching workflow tasks"

$tasksArray = GetWorkflowTasks
$global:operatingArray = $tasksArray.value

$count = $global:operatingArray.Count
Write-Host "Fetched $count tasks"

# overall process:
# - report total tasks count
# - prompt for project selection
# - report filtered tasks count
# - prompt for dates
# - report filtered tasks count
# - prompt for go-ahead
# - remove tasks
# - end

if ((YNPrompt "select project? [y/n]") -eq "y")
{
  # select project
  # report filtered count
  $selection = ChooseProject 
  $projectId = $selection.publishId
  $projectName = $selection.name
  $global:operatingArray = $global:operatingArray | Where-Object { ($_.publishId -eq $projectId) }
  $count = $global:operatingArray.Count
  Write-Host "$count tasks for project '$projectName' selected for deletion"
}

$start = $global:operatingArray[0].createdDateTime
$end = $global:operatingArray[-1].createdDateTime
Write-Host "Tasks exist between $start and $end"
if ((YNPrompt "filter by date range? [y/n]") -eq "y")
{
  # input dates
  # report filtered count
  $start = PromptForDate "start"
  $end = PromptForDate "end"
  $global:operatingArray = $global:operatingArray | Where-Object { ($_.createdDateTime -ge ([datetime]$start)) -and ($_.createdDateTime -le ([datetime]$end)) }
  $count = $global:operatingArray.Count
  Write-Host "$count tasks selected for deletion"
}

if ((YNPrompt "proceed with deleting $count tasks? [y/n]") -eq "y")
{
  Write-Host "Deleting $count tasks"
  # delete tasks
  foreach ($target in $global:operatingArray)
  {
    DeleteWorkflowTask $target.id
  }
  Write-Host "Finished deletion"
}
