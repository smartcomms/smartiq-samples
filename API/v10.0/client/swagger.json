﻿{
  "swagger": "2.0",
  "info": {
    "version": "v1",
    "title": "Infiniti API"
  },
  "basePath": "/api/v1/client",
  "produces": [
    "application/json"
  ],
  "consumes": [
    "application/json"
  ],
  "parameters": {
    "skip": {
      "name": "$skip",
      "in": "query",
      "description": "Skip this number of rows",
      "required": false,
      "type": "integer",
      "format": "int32"
    },
    "top": {
      "name": "$top",
      "in": "query",
      "description": "Return this number of rows",
      "required": false,
      "type": "integer",
      "format": "int32"
    }
  },
  "paths": {
    "/login/forms": {
      "post": {
        "summary": "Retrieve a session token via forms authentication",
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "description": "Login",
            "required": true,
            "schema": {
              "type": "object",
              "properties": {
                "userName": { "type": "string" },
                "password": { "type": "string" }
              }
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Success",
            "schema": {
              "$ref": "#/definitions/session"
            }
          },
          "403": {
            "description": "Disabled access"
          },
          "401": {
            "description": "Invalid login"
          }
        }
      }
    },
    "/login/account": {
      "post": {
        "summary": "Retrieve a session token via Windows authentication",
        "responses": {
          "200": {
            "description": "Success",
            "schema": {
              "$ref": "#/definitions/session"
            }
          },
          "403": {
            "description": "Disabled access"
          },
          "401": {
            "description": "Invalid login"
          }
        }
      }
    },
    "/login/samlredirect": {
      "post": {
        "summary": "Retrieve a session token via SAML authentication",
        "responses": {
          "302": {
            "description": "Beginning SAML redirection"
          },
          "405": {
            "description": "SAML not enabled"
          }
        }
      }
    },
    "/login/cookietosession": {
      "post": {
        "summary": "Retrieve a session token via authentication cookie",
        "responses": {
          "200": {
            "description": "Success",
            "schema": {
              "$ref": "#/definitions/session"
            }
          },
          "403": {
            "description": "Disabled access"
          },
          "401": {
            "description": "Invalid login"
          }
        }
      }
    },

    "/generate/{id}": {
      "parameters": [
        {
          "name": "id",
          "in": "path",
          "description": "Publish Id (Project Group Guid)",
          "required": true,
          "type": "string"
        }
      ],
      "post": {
        "parameters": [
          {
            "in": "body",
            "name": "Generation details",
            "required": false,
            "schema": {
              "type": "object",
              "properties": {
                "options": {
                  "type": "object",
                  "properties": {
                    "version": {
                      "type": "string"
                    },
                    "generationDateTime": {
                      "type": "string",
                      "format": "date-time"
                    },
                    "log": {
                      "type": "boolean"
                    }
                  }
                },
                "data": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "dataServiceGuid": {
                        "type": "string"
                      },
                      "value": {
                        "type": "string"
                      }
                    }
                  }
                },
                "answerFile": {
                  "type": "string"
                },
                "values": {
                  "type": "object",
                  "additionalProperties": {
                    "type": "string"
                  }
                },
                "workflow": {
                  "type": "object",
                  "properties": {
                    "comment": { "type": "string" },
                    "sendToUserGuid": { "type": "string" },
                    "sendToUserName": { "type": "string" },
                    "sendToGroupGuid": { "type": "string" },
                    "sendToGroupName": { "type": "string" },
                    "nextState": { "type": "string" }
                  }
                },
                "schedule": {
                  "type": "object",
                  "properties": {
                    "startDateTime": {
                      "type": "string",
                      "format": "date-time"
                    }
                  }
                }
              }
            }
          }
        ],
        "summary": "Generates a form",
        "responses": {
          "202": {
            "description": "Generation request accepted",
            "headers": {
              "location": {
                "type": "string",
                "description": "Operations url to optionally poll results and completion"
              }
            }
          },
          "400": {
            "description": "Invalid input",
            "schema": {
              "$ref": "#/definitions/ErrorModel"
            }
          }
        }
      }
    },

    "/operations/{id}": {
      "parameters": [
        {
          "name": "id",
          "in": "path",
          "description": "Job Id",
          "required": true,
          "type": "string"
        }
      ],
      "get": {
        "responses": {
          "200": {
            "description": "An operation",
            "headers": {
              "Retry-After": {
                "type": "integer"
              }
            },
            "schema": {
              "type": "object",
              "properties": {
                "createdDate": {
                  "type": "string",
                  "format": "date-time"
                },
                "status": {
                  "type": "string",
                  "enum": [
                    "notStarted",
                    "running",
                    "succeeded",
                    "failed",
                    "cancelled"
                  ]
                },
                "messages": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "description": {
                        "type": "string"
                      },
                      "severity": {
                        "type": "string",
                        "enum": [
                          "info",
                          "warning",
                          "error"
                        ]
                      }
                    }
                  }
                },
                "resourceLocation": {
                  "type": "string"
                }
              }
            }
          }
        }
      },
      "delete": {
        "responses": {
          "204": {
            "description": "Generation cancelled"
          },
          "405": {
            "description": "Cancellation not allowed"
          }
        }
      }
    },

    "/generatedoutput/{id}": {
      "parameters": [
        {
          "name": "id",
          "in": "path",
          "description": "Job Id",
          "required": true,
          "type": "string"
        }
      ],
      "get": {
        "responses": {
          "200": {
            "description": "A generate output detail",
            "schema": {
              "type": "object",
              "properties": {
                "files": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "filename": {
                        "type": "string"
                      },
                      "resourceLocation": {
                        "type": "string"
                      }
                    }
                  }
                },
                "@nextLink": {
                  "type": "string"
                }
              }
            }
          }
        }
      }
    },
    "/generatedoutput/{id}/file/{fileid}": {
      "parameters": [
        {
          "name": "id",
          "in": "path",
          "description": "Job Id",
          "required": true,
          "type": "string"
        },
        {
          "name": "fileid",
          "in": "path",
          "description": "File Id",
          "required": true,
          "type": "string"
        }
      ],
      "get": {
        "responses": {
          "200": {
            "description": "A generated file",
            "schema": {
              "type": "file"
            }
          }
        }
      }
    },

    "/answerfiles": {
      "parameters": [
        {
          "$ref": "#/parameters/skip"
        },
        {
          "$ref": "#/parameters/top"
        }
      ],
      "get": {
        "summary": "Get a user's answer files",
        "responses": {
          "200": {
            "description": "A list of answer files",
            "schema": {
              "type": "object",
              "properties": {
                "value": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "id": {
                        "type": "string"
                      },
                      "description": {
                        "type": "string"
                      },
                      "runDate": {
                        "type": "string",
                        "format": "date-time"
                      },
                      "publishId": {
                        "type": "string"
                      }
                    }
                  }
                },
                "@nextLink": {
                  "type": "string"
                }
              }
            }
          }
        }
      },
      "post": {
        "parameters": [
          {
            "name": "Answer file",
            "in": "body",
            "required": true,
            "schema": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "publishId": {
                  "type": "string"
                },
                "answerXml": {
                  "type": "string"
                }
              },
              "required": [ "description", "publishId", "answerXml" ]
            }
          }
        ],
        "summary": "Creates an answer file",
        "responses": {
          "201": {
            "description": "Answer file created"
          },
          "400": {
            "description": "Invalid input",
            "schema": {
              "$ref": "#/definitions/ErrorModel"
            }
          }
        }
      }
    },
    "/answerfiles/{id}": {
      "parameters": [
        {
          "name": "id",
          "in": "path",
          "description": "Answer file guid",
          "required": true,
          "type": "string"
        }
      ],
      "get": {
        "summary": "Get specific answer file",
        "responses": {
          "200": {
            "description": "An answer file",
            "schema": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "runDate": {
                  "type": "string",
                  "format": "date-time"
                },
                "publishId": {
                  "type": "string"
                },
                "answerXml": {
                  "type": "string"
                }
              }
            }
          }
        }
      },
      "delete": {
        "summary": "Deletes an answer file",
        "responses": {
          "204": {
            "description": "Answer file deleted"
          }
        }
      }
    },

    "/inprogressanswerfiles": {
      "parameters": [
        {
          "$ref": "#/parameters/skip"
        },
        {
          "$ref": "#/parameters/top"
        }
      ],
      "get": {
        "summary": "Get a user's in-progress answer files",
        "responses": {
          "200": {
            "description": "A list of in-progress answer files",
            "schema": {
              "type": "object",
              "properties": {
                "value": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "id": {
                        "type": "string"
                      },
                      "description": {
                        "type": "string"
                      },
                      "runDate": {
                        "type": "string",
                        "format": "date-time"
                      },
                      "publishId": {
                        "type": "string"
                      }
                    }
                  }
                },
                "@nextLink": {
                  "type": "string"
                }
              }
            }
          }
        }
      },
      "post": {
        "parameters": [
          {
            "name": "Answer file",
            "in": "body",
            "required": true,
            "schema": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "publishId": {
                  "type": "string"
                },
                "answerXml": {
                  "type": "string"
                }
              },
              "required": [ "description", "publishId", "answerXml" ]
            }
          }
        ],
        "summary": "Creates an in-progress answer file",
        "responses": {
          "201": {
            "description": "In-progress answer file created"
          },
          "400": {
            "description": "Invalid input",
            "schema": {
              "$ref": "#/definitions/ErrorModel"
            }
          }
        }
      }
    },
    "/inprogressanswerfiles/{id}": {
      "parameters": [
        {
          "name": "id",
          "in": "path",
          "description": "Answer file guid",
          "required": true,
          "type": "string"
        }
      ],
      "get": {
        "summary": "Get specific in-progress answer file",
        "responses": {
          "200": {
            "description": "An answer file",
            "schema": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "runDate": {
                  "type": "string",
                  "format": "date-time"
                },
                "publishId": {
                  "type": "string"
                },
                "answerXml": {
                  "type": "string"
                }
              }
            }
          }
        }
      },
      "patch": {
        "parameters": [
          {
            "name": "Answer file",
            "in": "body",
            "required": true,
            "schema": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "answerXml": {
                  "type": "string"
                }
              }
            }
          }
        ],
        "summary": "Updates an in-progress answer file",
        "responses": {
          "204": {
            "description": "In-progress answer file created"
          },
          "400": {
            "description": "Invalid input",
            "schema": {
              "$ref": "#/definitions/ErrorModel"
            }
          }
        }
      },
      "delete": {
        "summary": "Deletes an in-progress answer file",
        "responses": {
          "204": {
            "description": "In-progress answer file deleted"
          }
        }
      }
    },

    "/forms": {
      "get": {
        "summary": "Get available forms",
        "responses": {
          "200": {
            "description": "A list of available forms",
            "schema": {
              "type": "object",
              "properties": {
                "folders": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "id": {
                        "type": "string"
                      },
                      "name": {
                        "type": "string"
                      },
                      "forms": {
                        "type": "array",
                        "items": {
                          "type": "object",
                          "properties": {
                            "id": {
                              "type": "string"
                            },
                            "name": {
                              "type": "string"
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    },

    "/workflow": {
      "get": {
        "summary": "Get available workflow items",
        "responses": {
          "200": {
            "description": "A list of workflow items",
            "schema": {
              "type": "object",
              "properties": {
                "value": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "id": {
                        "type": "string"
                      },
                      "assignedDateTime": {
                        "type": "string",
                        "format": "date-time"
                      },
                      "assignedBy": {
                        "type": "string"
                      },
                      "assignedType": {
                        "type": "string",
                        "enum": [
                          "User",
                          "Group"
                        ]
                      },
                      "comment": {
                        "type": "string"
                      },
                      "publishId": {
                        "type": "string"
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    },
    "/workflow/{id}": {
      "parameters": [
        {
          "name": "id",
          "in": "path",
          "description": "Workflow state guid",
          "required": true,
          "type": "string"
        }
      ],
      "patch": {
        "parameters": [
          {
            "name": "Workflow item",
            "in": "body",
            "required": true,
            "schema": {
              "type": "object",
              "properties": {
                "locked": {
                  "type": "boolean"
                }
              }
            }
          }
        ],
        "summary": "Unlocks a group workflow task",
        "responses": {
          "204": {
            "description": "Workflow updated"
          },
          "400": {
            "description": "Invalid input",
            "schema": {
              "$ref": "#/definitions/ErrorModel"
            }
          }
        }
      },
      "delete": {
        "summary": "Cancels a workflow task",
        "responses": {
          "204": {
            "description": "Workflow aborted"
          }
        }
      }
    },

    "/workflow/{id}/reassign": {
      "parameters": [
        {
          "name": "id",
          "in": "path",
          "description": "Workflow state guid",
          "required": true,
          "type": "string"
        }
      ],
      "post": {
        "parameters": [
          {
            "name": "Reassignment details",
            "in": "body",
            "required": true,
            "schema": {
              "type": "object",
              "properties": {
                "comment": {
                  "type": "string"
                },
                "assignToGuid": {
                  "type": "string"
                },
                "assignToName": {
                  "type": "string"
                }
              }
            }
          }
        ],
        "summary": "Reassigns a workflow task to another user",
        "responses": {
          "201": {
            "description": "Workflow reassigned"
          },
          "400": {
            "description": "Invalid input",
            "schema": {
              "$ref": "#/definitions/ErrorModel"
            }
          },
          "405": {
            "description": "Workflow does not allow reassignment"
          }
        }
      }
    }
  },

  "definitions": {
    "ErrorModel": {
      "type": "object",
      "required": [ "message", "code" ],
      "properties": {
        "message": {
          "type": "string"
        },
        "code": {
          "type": "string"
        },
        "target": {
          "type": "string"
        },
        "innererror": {
          "$ref": "#/definitions/InnerErrorModel"
        }
      }
    },
    "InnerErrorModel": {
      "type": "object",
      "required": [ "message", "code" ],
      "properties": {
        "message": {
          "type": "string"
        },
        "code": {
          "type": "string"
        },
        "innererror": {
          "$ref": "#/definitions/InnerErrorModel"
        }
      }
    },
    "session": {
      "type": "object",
      "properties": {
        "authorizationToken": {
          "type": "string"
        },
        "userGuid": {
          "type": "string"
        },
        "userName": {
          "type": "string"
        },
        "produceVersion": {
          "type": "string"
        }
      }
    }
  }
}
