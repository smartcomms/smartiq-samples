﻿{
  "swagger": "2.0",
  "info": {
    "version": "v1",
    "title": "Infiniti Client API",
    "description": "Allows to perform Produce tasks related with workflow, forms, answerFiles, etc."
  },
  "securityDefinitions": {
    "Bearer": {
      "type": "apiKey",
      "name": "Authorization",
      "in": "header"
    }
  },
  "basePath": "/api/v1/client",
  "produces": [
    "application/json"
  ],
  "consumes": [
    "application/json"
  ],
  "parameters": {
    "skip": {
      "name": "$skip",
      "in": "query",
      "description": "Skip this number of rows",
      "required": false,
      "type": "integer",
      "format": "int32"
    },
    "top": {
      "name": "$top",
      "in": "query",
      "description": "Return this number of rows",
      "required": false,
      "type": "integer",
      "format": "int32"
    }
  },
  "paths": {
    "/login/forms": {
      "post": {
        "summary": "Forms Authentication",
        "description": "<br>\n\n##/login/forms\n\nThis method supports authenticating a user and retrieving a session token via Forms Authentication.",
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "description": "Login",
            "required": true,
            "schema": {
              "type": "object",
              "properties": {
                "userName": {
                  "type": "string"
                },
                "password": {
                  "type": "string"
                }
              }
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Success",
            "schema": {
              "$ref": "#/definitions/session"
            }
          },
          "401": {
            "description": "Invalid login"
          },
          "403": {
            "description": "Disabled access"
          }
        }
      }
    },
    "/login/account": {
      "post": {
        "summary": "Windows Authentication",
        "description": "<br>\n\n##/login/account\n\nThis method supports authenticating a user and retrieving a session token via Windows Authentication. For this type of authentication, there is no need to supply explicit username/password credentials since ASP.NET and IIS can automatically retrieve and validate the Windows username of the end-user who is currently logged on to the Windows operating system and authenticate them in a secure way.",
        "responses": {
          "200": {
            "description": "Success",
            "schema": {
              "$ref": "#/definitions/session"
            }
          },
          "401": {
            "description": "Invalid login"
          },
          "403": {
            "description": "Disabled access"
          }
        }
      }
    },
    "/login/samlredirect": {
      "post": {
        "summary": "SAML Authentication",
        "description": "<br>\n\n##/login/samlredirect\n\nThis method supports authenticating a user and retrieving a session token via SAML Authentication. Authentication has been setup to return a `302` SAML redirect to an IDP so users can enter their credentials, similar to the redirect flow that Produce executes. The caller also needs to handle any UI that the IDP might display. The end of the redirects is a request to `/login/cookietosession` that will return session information.",
        "responses": {
          "302": {
            "description": "Beginning SAML redirection"
          },
          "405": {
            "description": "SAML not enabled"
          }
        }
      }
    },
    "/login/cookietosession": {
      "post": {
        "summary": "Cookie Authentication",
        "description": "<br>\n\n##/login/cookietosession\n\nThis method supports retrieving a session token via Authentication Cookie. This requires that the Manage or Produce cookies be sent with the request. The main scenarios are for use with the SAML redirect or UI within Produce.",
        "responses": {
          "200": {
            "description": "Success",
            "schema": {
              "$ref": "#/definitions/session"
            }
          },
          "401": {
            "description": "Invalid login"
          },
          "403": {
            "description": "Disabled access"
          }
        }
      }
    },
    "/generate/{id}": {
      "parameters": [
        {
          "name": "id",
          "in": "path",
          "description": "Publish Id (Project Group Guid)",
          "required": true,
          "type": "string"
        }
      ],
      "post": {
        "security": [
          {
            "Bearer": []
          }
        ],
        "parameters": [
          {
            "in": "body",
            "name": "Generation details",
            "required": false,
            "schema": {
              "type": "object",
              "properties": {
                "options": {
                  "type": "object",
                  "properties": {
                    "version": {
                      "type": "string"
                    },
                    "generationDateTime": {
                      "type": "string",
                      "format": "date-time"
                    },
                    "log": {
                      "type": "boolean"
                    }
                  }
                },
                "data": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "dataServiceGuid": {
                        "type": "string"
                      },
                      "value": {
                        "type": "string"
                      }
                    }
                  }
                },
                "answerFile": {
                  "type": "string"
                },
                "values": {
                  "type": "object",
                  "additionalProperties": {
                    "type": "string"
                  }
                },
                "workflow": {
                  "type": "object",
                  "properties": {
                    "comment": {
                      "type": "string"
                    },
                    "sendToUserGuid": {
                      "type": "string"
                    },
                    "sendToUserName": {
                      "type": "string"
                    },
                    "sendToGroupGuid": {
                      "type": "string"
                    },
                    "sendToGroupName": {
                      "type": "string"
                    },
                    "nextState": {
                      "type": "string"
                    }
                  }
                },
                "schedule": {
                  "type": "object",
                  "properties": {
                    "startDateTime": {
                      "type": "string",
                      "format": "date-time"
                    }
                  }
                }
              }
            }
          }
        ],
        "summary": "Generates a form",
        "description": "<br>\n\n##/generate/{id}\n\nThis method supports running an Infiniti project in an automated non-UI fashion.",
        "responses": {
          "202": {
            "description": "Generation request accepted",
            "headers": {
              "location": {
                "type": "string",
                "description": "Operations url to optionally poll results and completion"
              }
            }
          },
          "400": {
            "description": "Invalid input",
            "schema": {
              "$ref": "#/definitions/ErrorModel"
            }
          },
          "401": {
            "description": "Invalid Authorization-Token"
          }
        }
      }
    },
    "/operations/{id}": {
      "parameters": [
        {
          "name": "id",
          "in": "path",
          "description": "Job Id",
          "required": true,
          "type": "string"
        }
      ],
      "get": {
        "security": [
          {
            "Bearer": []
          }
        ],
        "summary": "Get resources state",
        "description": "<br>\n\n##/operations/{id}\n\nA resources state is a user addressable resource that tracks a long-running operation. The `GET` operation returns the resources state and any relevant extended information.\nThe following states are supported:\n`notStarted`\n\n`running`\n\n`succeeded`\n\n`failed`\n\nForms generations support an additional state\n\n`cancelled`\n",
        "responses": {
          "200": {
            "description": "An operation",
            "headers": {
              "Retry-After": {
                "type": "integer"
              }
            },
            "schema": {
              "type": "object",
              "properties": {
                "createdDate": {
                  "type": "string",
                  "format": "date-time"
                },
                "status": {
                  "type": "string",
                  "enum": [
                    "notStarted",
                    "running",
                    "succeeded",
                    "failed",
                    "cancelled"
                  ]
                },
                "messages": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "description": {
                        "type": "string"
                      },
                      "severity": {
                        "type": "string",
                        "enum": [
                          "info",
                          "warning",
                          "error"
                        ]
                      }
                    }
                  }
                },
                "resourceLocation": {
                  "type": "string"
                }
              }
            }
          }
        }
      },
      "delete": {
        "security": [
          {
            "Bearer": []
          }
        ],
        "summary": "Delete resources",
        "description": "<br>\n\n##/operations/{id}\n\nOperations may support the `DELETE` action. For example, generations that are in the `notStarted` state can be deleted. Operations that cannot be deleted will return `405 Method Not Allowed`.\n",
        "responses": {
          "204": {
            "description": "Generation cancelled"
          },
          "401": {
            "description": "Invalid Authorization-Token"
          },
          "405": {
            "description": "Cancellation not allowed"
          }
        }
      }
    },
    "/generatedoutput/{id}": {
      "parameters": [
        {
          "name": "id",
          "in": "path",
          "description": "Job Id",
          "required": true,
          "type": "string"
        }
      ],
      "get": {
        "security": [
          {
            "Bearer": []
          }
        ],
        "summary": "Get generated outputs",
        "description": "<br>\n\n##/generatedoutput/{id}\n\nThis method supports retrieving a list of generated documents and their location. Once the locations of generated documents are obtained, subsequent calls to the location url (`resourceLocation` url) can be made to retrieve the generated documents binaries.",
        "responses": {
          "200": {
            "description": "A list of generated documents and their location",
            "schema": {
              "type": "object",
              "properties": {
                "files": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "filename": {
                        "type": "string"
                      },
                      "resourceLocation": {
                        "type": "string"
                      }
                    }
                  }
                },
                "@nextLink": {
                  "type": "string"
                }
              }
            }
          },
          "401": {
            "description": "Invalid Authorization-Token"
          }
        }
      }
    },
    "/generatedoutput/{id}/file/{fileid}": {
      "parameters": [
        {
          "name": "id",
          "in": "path",
          "description": "Job Id",
          "required": true,
          "type": "string"
        },
        {
          "name": "fileid",
          "in": "path",
          "description": "File Id",
          "required": true,
          "type": "string"
        }
      ],
      "get": {
        "security": [
          {
            "Bearer": []
          }
        ],
        "summary": "Get generated output binaries",
        "description": "<br>\n\n##/generatedoutput/{id}/file/{fileid}\n\nReturns generated document binary.",
        "responses": {
          "200": {
            "description": "A generated file",
            "schema": {
              "type": "file"
            }
          },
          "401": {
            "description": "Invalid Authorization-Token"
          }
        }
      }
    },
    "/answerfiles": {
      "parameters": [
        {
          "$ref": "#/parameters/skip"
        },
        {
          "$ref": "#/parameters/top"
        }
      ],
      "get": {
        "security": [
          {
            "Bearer": []
          }
        ],
        "summary": "Get user's available answer files",
        "description": "<br>\n\n##/answerfiles\n\nThis method supports retrieving a list of user's saved Answer Files. The Answer Files retrieved by this method have been saved by the end user from the Finish Page, after completing a form in Produce.\nBy default, a call to this method will return a list of all user's saved Answer Files. To make sure responses are easier to handle, client can request for sets of paginated items by passing these query parameters:\n\n`$skip`, skip this number of rows.\n\n`$top`,return this number of rows.\n\nBy doing this the response will include a `@nextLink` flag pointing to the next page of results.",
        "responses": {
          "200": {
            "description": "A list of answer files",
            "schema": {
              "type": "object",
              "properties": {
                "value": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "id": {
                        "type": "string"
                      },
                      "description": {
                        "type": "string"
                      },
                      "runDate": {
                        "type": "string",
                        "format": "date-time"
                      },
                      "publishId": {
                        "type": "string"
                      }
                    }
                  }
                },
                "@nextLink": {
                  "type": "string"
                }
              }
            }
          },
          "401": {
            "description": "Invalid Authorization-Token"
          }
        }
      },
      "post": {
        "security": [
          {
            "Bearer": []
          }
        ],
        "parameters": [
          {
            "name": "Answer file",
            "in": "body",
            "required": true,
            "schema": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "publishId": {
                  "type": "string"
                },
                "answerXml": {
                  "type": "string"
                }
              },
              "required": [
                "description",
                "publishId",
                "answerXml"
              ]
            }
          }
        ],
        "summary": "Creates an answer file",
        "description": "<br>\n\n##/answerfiles\n\nThis method supports creating an Answer File. To make a request to this endpoint, a body of data will need to include these body parameters:\n\n`description`, title of the saved Answer File.\n\n`publishId`, unique Id of the published project.\n\n`answerXml`, Answer File XML.",
        "responses": {
          "201": {
            "description": "Answer file created"
          },
          "400": {
            "description": "Invalid input",
            "schema": {
              "$ref": "#/definitions/ErrorModel"
            }
          },
          "401": {
            "description": "Invalid Authorization-Token"
          }
        }
      }
    },
    "/answerfiles/{id}": {
      "parameters": [
        {
          "name": "id",
          "in": "path",
          "description": "Answer file guid",
          "required": true,
          "type": "string"
        }
      ],
      "get": {
        "security": [
          {
            "Bearer": []
          }
        ],
        "summary": "Get specific answer file",
        "description": "<br>\n\n##/answerfiles/{id}\n\nThis method supports retrieving an Answer File's details, including its XML.",
        "responses": {
          "200": {
            "description": "An answer file",
            "schema": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "runDate": {
                  "type": "string",
                  "format": "date-time"
                },
                "publishId": {
                  "type": "string"
                },
                "answerXml": {
                  "type": "string"
                }
              }
            }
          },
          "401": {
            "description": "Invalid Authorization-Token"
          }
        }
      },
      "delete": {
        "security": [
          {
            "Bearer": []
          }
        ],
        "summary": "Deletes an answer file",
        "description": "<br>\n\n##/answerfiles/{id}\n\nThis method supports deleting an Answer File.",
        "responses": {
          "204": {
            "description": "Answer file deleted"
          },
          "401": {
            "description": "Invalid Authorization-Token"
          }
        }
      }
    },
    "/inprogressanswerfiles": {
      "parameters": [
        {
          "$ref": "#/parameters/skip"
        },
        {
          "$ref": "#/parameters/top"
        }
      ],
      "get": {
        "security": [
          {
            "Bearer": []
          }
        ],
        "summary": "Get a user's in-progress answer files",
        "description": "<br>\n\n##/inprogressanswerfiles\n\nThis method supports retrieving a list of user's in-progress Answer Files. The in-progress Answer Files retrieved by this method have been saved by the end user when a form is mid way through its execution cycle and an \"In-Progress Save\" is required.\nBy default, a call to this method will return a list of all user's in-progress Answer Files. To make sure responses are easier to handle, client can request for sets of paginated items by passing these query parameters:\n\n`$skip`, skip this number of rows.\n\n`$top`,return this number of rows.\n\nBy doing this the response will include a `@nextLink` flag pointing to the next page of results.",
        "responses": {
          "200": {
            "description": "A list of in-progress answer files",
            "schema": {
              "type": "object",
              "properties": {
                "value": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "id": {
                        "type": "string"
                      },
                      "description": {
                        "type": "string"
                      },
                      "runDate": {
                        "type": "string",
                        "format": "date-time"
                      },
                      "publishId": {
                        "type": "string"
                      }
                    }
                  }
                },
                "@nextLink": {
                  "type": "string"
                }
              }
            }
          },
          "401": {
            "description": "Invalid Authorization-Token"
          }
        }
      },
      "post": {
        "security": [
          {
            "Bearer": []
          }
        ],
        "parameters": [
          {
            "name": "Answer file",
            "in": "body",
            "required": true,
            "schema": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "publishId": {
                  "type": "string"
                },
                "answerXml": {
                  "type": "string"
                }
              },
              "required": [
                "description",
                "publishId",
                "answerXml"
              ]
            }
          }
        ],
        "summary": "Creates an in-progress answer file",
        "description": "<br>\n\n##/inprogressanswerfiles\n\nThis method supports creating an in-progress Answer File. To make a request to this endpoint, a body of data will need to include these body parameters:\n\n`description`, title of the in-progress Answer File.\n\n`publishId`, unique Id of the published project.\n\n`answerXml`, in-progress Answer File XML.",
        "responses": {
          "201": {
            "description": "In-progress answer file created"
          },
          "400": {
            "description": "Invalid input",
            "schema": {
              "$ref": "#/definitions/ErrorModel"
            }
          },
          "401": {
            "description": "Invalid Authorization-Token"
          }
        }
      }
    },
    "/inprogressanswerfiles/{id}": {
      "parameters": [
        {
          "name": "id",
          "in": "path",
          "description": "Answer file guid",
          "required": true,
          "type": "string"
        }
      ],
      "get": {
        "security": [
          {
            "Bearer": []
          }
        ],
        "summary": "Get specific in-progress answer file",
        "description": "<br>\n\n##/inprogressanswerfiles/{id}\n\nThis method supports retrieving an in-progress Answer File's details, including its XML.",
        "responses": {
          "200": {
            "description": "An answer file",
            "schema": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "runDate": {
                  "type": "string",
                  "format": "date-time"
                },
                "publishId": {
                  "type": "string"
                },
                "answerXml": {
                  "type": "string"
                }
              }
            }
          },
          "401": {
            "description": "Invalid Authorization-Token"
          }
        }
      },
      "patch": {
        "security": [
          {
            "Bearer": []
          }
        ],
        "parameters": [
          {
            "name": "Answer file",
            "in": "body",
            "required": true,
            "schema": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "answerXml": {
                  "type": "string"
                }
              }
            }
          }
        ],
        "summary": "Updates an in-progress answer file",
        "description": "<br>\n\n##/inprogressanswerfiles/{id}\n\nThis method supports updating an in-progress Answer File. To make a request to this endpoint, a body of data will need to include these body parameters:\n\n`description`, title of the in-progress Answer File.\n\n`answerXml`, in-progress Answer File XML.",
        "responses": {
          "204": {
            "description": "In-progress answer file created"
          },
          "400": {
            "description": "Invalid input",
            "schema": {
              "$ref": "#/definitions/ErrorModel"
            }
          },
          "401": {
            "description": "Invalid Authorization-Token"
          }
        }
      },
      "delete": {
        "security": [
          {
            "Bearer": []
          }
        ],
        "summary": "Deletes an in-progress answer file",
        "description": "<br>\n\n##/inprogressanswerfiles/{id}\n\nThis method supports deleting an in-progress Answer File.",
        "responses": {
          "204": {
            "description": "In-progress answer file deleted"
          },
          "401": {
            "description": "Invalid Authorization-Token"
          }
        }
      }
    },
    "/forms": {
      "get": {
        "security": [
          {
            "Bearer": []
          }
        ],
        "summary": "Get available forms",
        "description": "<br>\n\n##/forms\n\nThis method supports retrieving a list of the available folders and projects that the current user has permissions to run.",
        "responses": {
          "200": {
            "description": "A list of available forms",
            "schema": {
              "type": "object",
              "properties": {
                "folders": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "id": {
                        "type": "string"
                      },
                      "name": {
                        "type": "string"
                      },
                      "forms": {
                        "type": "array",
                        "items": {
                          "type": "object",
                          "properties": {
                            "id": {
                              "type": "string"
                            },
                            "name": {
                              "type": "string"
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          },
          "401": {
            "description": "Invalid Authorization-Token"
          }
        }
      }
    },
    "/workflow": {
      "get": {
        "security": [
          {
            "Bearer": []
          }
        ],
        "summary": "Get available workflow items",
        "description": "<br>\n\n##/workflow\n\nThis method supports retrieving list of the workflow tasks that are assigned to you or a group that you belong to. Items that assigned to a group have a boolean property indicating this as well as a flag to say whether you have it locked.\nDirectly assigned tasks can be cancelled via the `DELETE` verb. Whether this is successful depends on project design.\n\nLocked tasks can be unlocked via the `PATCH` verb.\n`id`, workflow state guid.\n\n`assignedDateTime`, time that the previous user assigned this task.\n\n`assignedBy`, user name of the assigning person.\n\n`assignedType`: `Group` if this is a group assigned task, otherwise `User`.\n \n`comment`, information left by the assigning person.\n\n`publishId`, id of the form to launch.\n\n`locked`: true if you have an exclusive lock on the group task.\n",
        "responses": {
          "200": {
            "description": "Returns a list of workflow items",
            "schema": {
              "type": "object",
              "properties": {
                "value": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "id": {
                        "type": "string"
                      },
                      "assignedDateTime": {
                        "type": "string",
                        "format": "date-time"
                      },
                      "assignedBy": {
                        "type": "string"
                      },
                      "assignedType": {
                        "type": "string",
                        "enum": [
                          "User",
                          "Group"
                        ]
                      },
                      "comment": {
                        "type": "string"
                      },
                      "publishId": {
                        "type": "string"
                      }
                    }
                  }
                }
              }
            }
          },
          "401": {
            "description": "Invalid Authorization-Token"
          }
        }
      }
    },
    "/workflow/{id}": {
      "parameters": [
        {
          "name": "id",
          "in": "path",
          "description": "Workflow state guid",
          "required": true,
          "type": "string"
        }
      ],
      "patch": {
        "security": [
          {
            "Bearer": []
          }
        ],
        "parameters": [
          {
            "name": "Workflow item",
            "in": "body",
            "required": true,
            "schema": {
              "type": "object",
              "properties": {
                "locked": {
                  "type": "boolean"
                }
              }
            }
          }
        ],
        "summary": "Unlocks a group workflow task",
        "description": "<br>\n\n##/workflow/{id}\n\nThis method supports unlocking a workflow task that are assigned a group that you belong to.",
        "responses": {
          "204": {
            "description": "Workflow updated"
          },
          "400": {
            "description": "Invalid input",
            "schema": {
              "$ref": "#/definitions/ErrorModel"
            }
          },
          "401": {
            "description": "Invalid Authorization-Token"
          }
        }
      },
      "delete": {
        "security": [
          {
            "Bearer": []
          }
        ],
        "summary": "Cancels a workflow task",
        "description": "<br>\n\n##/workflow/{id}\n\nThis method supports canceling a workflow task that are assigned to you.",
        "responses": {
          "204": {
            "description": "Workflow aborted"
          },
          "401": {
            "description": "Invalid Authorization-Token"
          }
        }
      }
    },
    "/workflow/{id}/reassign": {
      "parameters": [
        {
          "name": "id",
          "in": "path",
          "description": "Workflow state guid",
          "required": true,
          "type": "string"
        }
      ],
      "post": {
        "security": [
          {
            "Bearer": []
          }
        ],
        "parameters": [
          {
            "name": "Reassignment details",
            "in": "body",
            "required": true,
            "schema": {
              "type": "object",
              "properties": {
                "comment": {
                  "type": "string"
                },
                "assignToGuid": {
                  "type": "string"
                },
                "assignToName": {
                  "type": "string"
                }
              }
            }
          }
        ],
        "summary": "Reassigns a workflow task to another user",
        "description": "<br>\n\n##/workflow/{id}/reassign\n\nThis method supports reassigning a workflow item that is currently directly assigned to you.\n\n`comment`, optional. Comment for the assignment to the next user.\n\n`assignToGuid`, optional. The user GUID to assign the next state to.\n\n`assignToName`, optional. The user name to assign the next state to.If both `assignToGuid` and `assignToName` are supplied then the GUID will be used. At least one of these properties must be supplied.\n",
        "responses": {
          "201": {
            "description": "Workflow reassigned"
          },
          "400": {
            "description": "Invalid input",
            "schema": {
              "$ref": "#/definitions/ErrorModel"
            }
          },
          "401": {
            "description": "Invalid Authorization-Token"
          },
          "405": {
            "description": "Workflow does not allow reassignment"
          }
        }
      }
    }
  },
  "definitions": {
    "ErrorModel": {
      "type": "object",
      "required": [
        "message",
        "code"
      ],
      "properties": {
        "message": {
          "type": "string"
        },
        "code": {
          "type": "string"
        },
        "target": {
          "type": "string"
        },
        "innererror": {
          "$ref": "#/definitions/InnerErrorModel"
        }
      }
    },
    "InnerErrorModel": {
      "type": "object",
      "required": [
        "message",
        "code"
      ],
      "properties": {
        "message": {
          "type": "string"
        },
        "code": {
          "type": "string"
        },
        "innererror": {
          "$ref": "#/definitions/InnerErrorModel"
        }
      }
    },
    "session": {
      "type": "object",
      "properties": {
        "AuthorizationToken": {
          "type": "string"
        },
        "UserGuid": {
          "type": "string"
        },
        "UserName": {
          "type": "string"
        },
        "ProduceVersion": {
          "type": "string"
        }
      }
    }
  }
}
