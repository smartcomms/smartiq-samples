Please read these Terms of Use (this “Agreement”) carefully. It contains important terms about your rights to use this Platform and our liability to you in respect of the Platform. 

This Agreement is between the company or organisation for which you are an authorised representative (“you”) and SmartComms SC Limited which is registered in England and whose registered office is at Catalyst House, 720 Centennial Court, Centennial Park, Elstree, Hertfordshire WD6 3SY (“SmartCom” or “we” or “us”). The Agreement applies to your use of (including any access to and downloading from) SmartCom’s Conversation Cloud Marketplace (together with any materials and services available therein, and any subsequent versions of the Marketplace platform, the “Platform”). 

BY DOWNLOADING ANYTHING FROM THIS REPOSITORY, YOU AGREE TO BE BOUND BY THIS AGREEMENT AND AFFIRM THAT YOU ARE OF LEGAL AGE WHERE YOU LIVE AND HAVE THE LEGAL CAPACITY TO ENTER INTO THIS AGREEMENT AS AN AUTHORISED REPRESENTATIVE OF YOUR COMPANY. 

1. YOUR RIGHTS 

1.1 Subject to your compliance with this Agreement, and for so long as you are permitted by SmartCom to use the Platform, you may view and use any portion of the Platform to which we provide you access under this Agreement, solely in accordance with the functionality that we make available to you. You use the Platform at your own risk, and we make no representations or warranties that the Platform will be suitable for your intended use or any particular purpose. 

2. CHANGES 

2.1 We may change this Agreement from time to time by notifying you of such changes by the means specified for notices in Section 19 at least two weeks before they become effective. If you do not object to such changes becoming effective within that time period, your consent to such modification is deemed given. You will be informed about this effect of your silence in the notification. If you object to the modifications becoming effective, we may terminate the Agreement with immediate effect. Also, your clicking or tapping any button or box marked “Accept,” “Agree” or “OK” (or a similar term) in connection with being notified about the change and a reviewable version of the new terms will constitute your acceptance of such changes. The “Last Updated” legend above indicates when this Agreement was last changed. Any such changes will not apply to any dispute between you and us arising prior to the date on which the change becomes effective. It is your responsibility to ensure that you keep up-to-date with any changes to this Agreement, including its termination under Section 15 below, whether or not we provide prior notice. 

2.2 We may modify all or part of the Platform at any time. When making changes to the portion of the Platform used by you based on the terms of this Agreement, we shall take into account your reasonable interests. A change is deemed reasonably acceptable for you in particular if it is necessary to adapt the Platform to changed circumstances with regard to technological developments (in particular information security developments), market requirements as well as any changes in the applicable law, and in case of any new features, functions, or services added to the Platform. If a change to the Platform is not reasonably acceptable to you, you have the right to terminate this Agreement without notice and cease all further use of the Platform. 

2.3 At any time and without liability or prior notice, we may suspend or discontinue all or part of the Platform (including access to the Platform via any third-party links), or offer opportunities to some or all Platform users. We reserve the right to introduce new features or functionality for which the payment of fees may be required. 

3. INFORMATION SUBMITTED THROUGH THE PLATFORM 

3.1 Your submission of information through the Platform is governed by this Agreement and the Platform’s Privacy Policy, located at https://www.smartcommunications.com/privacycookie-policy/ (the “Privacy Policy”). You represent and warrant that any information which you provide in connection with the Platform is and will remain accurate and complete, and that you will maintain and update such information as needed. 

4. JURISDICTIONAL ISSUES 

4.1 The Platform may not be appropriate or available for use in some jurisdictions. Any use of the Platform is at your own risk, and you must comply with all applicable laws, rules and regulations in doing so. We may limit the Platform’s availability at any time, in whole or in part, to any person, geographic area or jurisdiction that we choose. 

5. REGISTRATION; USER NAMES AND PASSWORDS 

5.1 You may need to register or otherwise enter a user name, password or other data, information or credential with respect to the Platform (collectively, “Access Credentials”), to use all or part of the Platform. 

5.2 You may obtain a number of different Access Credentials for the people within your business who are authorised to use the Platform (“Authorised Users”). You must notify us of each individual within your business who is an Authorised User, and each Authorised User must have their own Access Credentials, and must comply with the terms of this Agreement. 

5.3 We may reject, or require that you change, any Access Credential that you provide. 

5.4 Access Credentials are for personal use only in accordance with this Agreement and each Authorised User should keep them confidential; you and each Authorised User, and not SmartCom, are responsible for any use or misuse of your Access Credentials and, in each case, you must promptly notify us of any actual or suspected confidentiality breach or unauthorized use of your Access Credentials or your Platform account. 

5.5 You are responsible for obtaining, maintaining and paying for all hardware and all telecommunications and other services needed for you to use the Platform (including any applicable device data transmission charges). 

6. RULES OF CONDUCT 

6.1 In connection with the Platform, you must not: 

(a) post, transmit or otherwise make available through or in connection with the Platform any materials that are or may be: (i) threatening, harassing, degrading, hateful or intimidating, or otherwise fail to respect the rights and dignity of others; (ii) defamatory, libellous, fraudulent or otherwise tortious; (iii) obscene, indecent, pornographic or otherwise objectionable; or (iv) protected by copyright, trademark, trade secret, right of publicity or privacy or any other proprietary right, without the express prior written consent of the applicable owner; 

(b) post, transmit or otherwise make available through or in connection with the Platform any virus, worm, Trojan horse, Easter egg, time bomb, spyware or other computer code, file or program that is or is potentially harmful or invasive or intended to damage or hijack the operation of, or to monitor the use of, any hardware, software or equipment (each, a “Virus”); 

(c) use the Platform for any purpose that is fraudulent or otherwise tortious or unlawful; 

(d) harvest or collect information about users of the Platform; 

(e) use the Platform for any commercial solicitation purposes, or transmit through or in connection with the Platform, any spam, chain letters or other unsolicited communications; 

(f) interfere with or disrupt the operation of the Platform or the servers or networks used to make the Platform available, including by hacking or defacing any portion of the Platform (including any content available thereby); or violate any requirement, procedure or policy of such servers or networks; 

(g) restrict or inhibit any other person from using the Platform; 

(h) reproduce, modify, adapt, translate, create derivative works of, sell, rent, lease, loan, timeshare, distribute or otherwise exploit any portion of (or any use of) the Platform except as expressly authorized herein, without SmartCom’s express prior written consent; 

(i) reverse engineer, decompile or disassemble any portion of the Platform, except to the extent that such restriction is expressly prohibited by applicable law; 

(j) remove any copyright, trademark or other proprietary rights notice from the Platform; 

(k) frame or mirror any portion of the Platform, or otherwise incorporate any portion of the Platform into any product or service, without SmartCom’s express prior written consent; or 

(l) use any robot, spider, Platform search/retrieval application or other manual or automatic device to retrieve, index, “scrape,” “data mine” or otherwise gather Platform content (including User Submissions, as defined below), or reproduce or circumvent the navigational structure or presentation of the Platform, without SmartCom’s express prior written consent. 

7. RESOURCES 

7.1 The Platform makes available content, information, data, materials, services, products, merchandise, functionality or other resources (collectively, “Resources”), as well as references and links to such Resources. Resources may be made available and owned by SmartCom (“SmartCom Resources”) or by third parties, and are not intended to amount to advice on which you should rely. We make no representations as to the accuracy, validity, timeliness, completeness, reliability, integrity, quality, legality, usefulness or safety of any or all of the Resources and the Platform, or any intellectual property rights therein. Resources are subject to change at any time without notice. 

7.2 We disclaim all liability and responsibility arising from any reliance placed on any Resources by you or any other user of the Platform, or by anyone who may be informed of the content of any Resources, except with regard to defects of such Resources that were known by us and fraudulently not disclosed by us. 

7.3 Third Party Resources 

(a) Certain Platform functionality may make available access to Resources made available and owned by third parties (“Third Party Resources”), or allow for the routing or transmission of Third Party Resources, including via links. By using such functionality, you are directing us to access, route and transmit to you the applicable Third Party Resources. 

(b) We neither control nor endorse, nor are we responsible for, any Third Party Resources, including the accuracy, validity, timeliness, completeness, reliability, integrity, quality, legality, usefulness or safety of Third Party Resources, or any intellectual property rights therein. Certain Third Party Resources may, among other things, be inaccurate, misleading or deceptive. Nothing in this Agreement will be deemed to be a representation or warranty by SmartCom with respect to any Third Party Resources. We have no obligation to monitor Third Party Resources, and we may block or disable access to any Third Party Resources (in whole or part) through the Platform at any time. In addition, the availability of any Third Party Resources through the Platform does not imply our endorsement of, or our affiliation with, any provider of such Third Party Resources even if such Third Party Resources are marketed or distributed via the Platform or any of the other Resources provided by us, nor does such availability create any legal relationship between you and any such provider. 

(c) You agree and acknowledge that: 

(i) the relevant third parties, and not SmartCom, are solely responsible for the performance of their Resources (including technical support), the content on their websites and their use of your data; 

(ii) it is your responsibility to verify and accept any applicable terms of use or access for any Third Party Resources; 

(iii) SmartCom will not have any liability to you for third parties or Third Party Resources; and 

(iv) you will not use the Third Party Resources in any manner that would infringe or violate the rights of SmartCom or any other party. 

(d) Your use of Third Party Resources is at your own risk and is subject to any additional terms, conditions and policies applicable to such Third Party Resources (such as terms of service or privacy policies of the providers of such Third Party Resources). 

7.4 SmartCom Resources and Jointly Owned Resources 

(a) SmartCom Resources that are available on the Platform are offered by SmartCom subject to terms and conditions that accompany each Resource. Such Resources may use or require third party APIs or technology (“Jointly Owned Resources”). Where terms and conditions are not specified on a SmartCom website or expressly linked or associated with each such SmartCom Resource, you agree that: 

(i) your rights to use the SmartCom Resources are non-transferable, non-exclusive, revocable and terminable at any time; 

(ii) SmartCom Resources are provided “AS IS” and “AS AVAILABLE” without any express or implied guarantees and warranties; and 

(iii) SmartCom may modify the terms and conditions applicable to SmartCom Resources at any time. 

(b) If you do not agree with all terms, you do not have rights to use the SmartCom Resources, and you must immediately cease all use and access. Where Jointly Owned Resources are hosted or provided by Third Parties, the provisions of Section 7.3 of this Agreement shall apply. 

8. USER SUBMISSIONS 

8.1 Certain Platform functionality may provide you with the ability to make available certain materials or information through or in connection with the Platform (each, a “User Submission”). SmartCom has no control over and is not responsible for User Submissions, any use or misuse (including any distribution) by any third party of User Submissions or for any of your interactions with other Platform users. If you choose to make any of your personally identifiable or other information publicly available through the platform, you do so at your own risk. 

8.2 Your Submissions and Feedback 

(a) For purposes of clarity, you retain ownership of each User Submission that you post or submit to, or otherwise make available through or in connection with, the Platform (each, “Your Submission”). You hereby grant to us a non-exclusive, worldwide, royalty-free, fully paid-up, perpetual, irrevocable, transferable and fully sublicensable (through multiple tiers) licence, without additional consideration to you or any third party, to reproduce, distribute, perform and display (publicly or otherwise), create derivative works of, adapt, modify, store and otherwise use, analyze and exploit Your Submissions, in any format or media now known or hereafter developed, for the purposes of marketing and promoting the products and services of SmartCom or its affiliates and operating, promoting and improving the Platform. 

(b) You agree that Your Submissions will comply with the Rules of Conduct set out in Section 6 above and any submission guidelines or other rules SmartCom establishes that are posted on the Platform or that SmartCom otherwise makes available, and further agree that you will not post or submit to, or otherwise make available or in connection with, the Platform any User Submissions that: 

(i) may create a risk of harm, loss, physical or mental injury, emotional distress, death, disability, disfigurement, or physical or mental illness to you, to any other person, or to any animal; 

(ii) may create a risk of any other loss or damage to any person or property; 

(iii) seeks to harm or exploit children by exposing them to inappropriate content, asking for personally identifiable details or otherwise; 

(iv) may constitute or contribute to a crime or tort; 

(v) contains any information or content that we deem to be unlawful, harmful, abusive, racially or ethnically offensive, defamatory, infringing, invasive of personal privacy or publicity rights, harassing, humiliating to other people (publicly or otherwise), libelous, threatening, profane, or otherwise objectionable; 

(vi) contains any information or content that is illegal; 

(vii) contains any information or content that you do not have a right to make available under any law or under contractual or fiduciary relationships; or 

(viii) contains any information or content that you know is not correct and current; or 

(ix) violates any other applicable policy. 

(c) You agree that Your Submissions do not and will not violate third-party rights of any kind, including without limitation any intellectual property rights or rights of privacy. 

(d) SmartCom reserves the right, but is not obliged, to reject and/or remove any User Submissions that SmartCom believes, in its sole discretion, violates these provisions. 

(e) In addition, if you provide to us any ideas, proposals, suggestions or other materials (“Feedback”), whether related to the Platform or otherwise, such Feedback will be deemed Your Submission, and you hereby acknowledge and agree that such Feedback is not confidential, and that your provision of such Feedback is gratuitous, unsolicited and without restriction, and does not place SmartCom under any fiduciary or other obligation. 

(f) You represent and warrant that: 

(i) you have all rights necessary to grant the licences granted in this section; 

(ii) Your Submissions are complete and accurate; and 

(iii) Your Submissions and your provision thereof to us (whether through and in connection with the Platform or otherwise) are not fraudulent, tortious or otherwise in violation of any applicable law or any right of any third party (including any intellectual property, publicity or data protection rights). 

9. INTELLECTUAL PROPERTY RIGHTS 

9.1 You acknowledge that all intellectual property rights in the Platform and all Resources belong to us or our licensors. You have no rights in, or to, the Platform other than the right to access it in accordance with this Agreement. 

9.2 You are permitted to make use of the Platform and Resources, for example by accessing and downloading them, for your internal business purposes. All other use of the Platform, for example the storage or reproduction of (a part of) the Platform in any external site or the creation of links, hypertext links or deep links between the Platform and any other platform or internet site, is prohibited without the express written consent of SmartCom. Screen-scraping or web-scraping is prohibited without the express written consent of SmartCom. 

9.3 All trade names, trademarks, service marks, logos, symbols and copyrightable works available through the Platform are the property of their respective owners and, except for the rights that you grant to SmartCom under Section 8, nothing contained on the Platform or in this Agreement should be construed as granting any right to use any trade names, trade marks, service marks, logos or copyrightable works without the express prior written consent of the owner. 

9.4 You may not use our trade names, trade marks, service marks, logos, symbols and copyrightable works (including, but not limited to, SMART COMMUNICATIONS, SMARTCOMM, SMARTDX, SMARTCOMM FOR SALESFORCE, SCALE THE CONVERSATION, CONVERSATION CLOUD and DOCBOX) (“SmartCom Marks”) without our prior written consent. You agree not to use SmartCom Marks in connection with any product or service that is not ours, or in any manner that is likely to cause confusion. 

9.5 You have the right to use and download Third Party Resources for internal business purposes, but you may not use the trade names, trademarks, service marks, logos or copyrightable works of any third parties (“Third Party Marks”) to which you may have access as a result of your use of Third Party Resources without the express prior written consent of the owner of the relevant Third Party Mark. 

10. MONITORING 

10.1 Subject to applicable data protection law, we may (but have no obligation to) monitor, moderate and/or analyse your use of the Platform, and monitor, moderate, analyse, alter and/or remove User Submissions before or after they appear on the Platform. We may disclose information regarding your access to and use of the Platform, and the circumstances surrounding such access and use, to third parties as permitted by applicable data protection law. 

11. PROMOTIONS 

11.1 Any sweepstakes, contests, raffles, surveys, games, coupons, rebates or similar promotions (collectively, “Promotions”) made available through the Platform may be governed by rules that are separate from this Agreement. If you participate in any Promotions, please review the applicable rules as well as the Privacy Policy. If the rules for a Promotion conflict with this Agreement, the Promotion rules shall prevail. 

12. DISCLAIMER OF WARRANTIES 

12.1 The Platform and any Resources (including any Third Party Resources) are made available to you without any warranties of any kind, except with regard to defects that have been fraudulently concealed by SmartCom. To the extent permitted by law, we exclude all conditions, warranties, representations or other terms which may apply to the Platform or any content on it, whether express or implied. All disclaimers of any kind (including in this Section 12 and elsewhere in this Agreement) are made for the benefit of both SmartCom and its affiliates and their respective owners, directors, officers, employees, affiliates, agents, representatives, licensors, suppliers and service providers, and their respective successors and assigns (collectively, the “SmartCom Parties”). 

13. LIMITATION OF LIABILITY 

13.1 Unless otherwise agreed in writing between the Parties under applicable license terms signed by both Parties, the following provisions shall apply. 

13.2 All claims against SmartCom for compensation or damages and reimbursement of expenditures, regardless of the legal nature of the respective claim (e.g. contractual claims, claims arising from tortious acts or under competition law) (collectively the “Damages”) are subject to the limitations of liability set out in this Section 13. 

13.3 Nothing in this Agreement excludes or limits our liability for death or personal injury arising from our negligence, or our fraud or fraudulent misrepresentation, or any other liability that cannot be excluded or limited by English law. 

13.4 We will not be liable to you for any loss or damage, whether in contract, tort (including negligence), breach of statutory duty, or otherwise, even if foreseeable, arising under or in connection with use of, or inability to use, the Platform, or your use of or reliance on the Resources on the Platform. 

13.5 To the fullest extent permitted under applicable law: (a) no SmartCom Party will be liable either for any indirect or consequential damages of any kind arising out of or in connection with your use of or reliance on the Resources on the Platform, nor for any damages for diminution of value, loss of profits, loss of revenue, loss of business, loss of use or data, loss of goodwill, in either case whether under any contract, tort (including negligence), strict liability or other theory and even if advised in advance of the possibility of such damages or losses; (b) your sole and exclusive remedy for dissatisfaction with the Resources or the Platform is to stop using the Platform; and (c) the maximum aggregate liability of all SmartCom Parties, collectively, to you for all damages, losses and causes of action, whether in contract, tort (including negligence) or otherwise, will be equal to the greater of (i) the total amount, if any, paid by you to SmartCom in connection with this Agreement; and (ii) £100. You acknowledge and agree that no SmartCom Party has any liability under this Agreement to any Authorised User. 

13.6 We will not be liable for any loss or damage caused by a virus, distributed denial-of-service attack, or other technologically harmful material that may infect your computer equipment, computer programs, data or other proprietary material due to your use of the Platform or to its downloading of any content on the Platform, or on any website linked to the Platform. 

14. INDEMNITY 

14.1 To the fullest extent permitted under applicable law, you agree to defend, indemnify and hold harmless each SmartCom Party from and against all claims, liabilities, damages, judgments, awards, losses, costs, expenses and fees (including attorneys’ fees and expenses) arising out of or relating to: (a) your use of, or activities in connection with, the Platform (including Your Submissions); or (b) any infringement or alleged infringement of this Agreement by you. 

15. TERMINATION AND SUSPENSION 

15.1 This Agreement is effective until terminated. You may terminate the Agreement at any time and without prior notice. SmartCom may terminate this Agreement for convenience with two weeks’ prior notice. SmartCom’s right to terminate the Agreement for cause remains unaffected. SmartCom may suspend your right to use the Platform at any time and without prior notice if SmartCom reasonably believes that you have violated or acted inconsistently with this Agreement, or if SmartCom reasonably believes this to be necessary to preserve the security or integrity of the Platform. Upon any such termination or suspension becoming effective, your right to use the Platform will immediately cease. In case of a termination becoming effective, SmartCom may, without liability to you or any third party, immediately deactivate or delete your Access Credentials, Your Submissions and all associated materials, without any obligation to provide any further access to such materials. Any provision of this Agreement that expressly or by implication is intended to come into or continue in force on or after termination of this Agreement shall remain in full force and effect. 

16. GOVERNING LAW 

16.1 This Agreement, your use of (including any access to) the Platform and all related matters are governed solely by, and construed solely in accordance with, the laws of the England and Wales. You and SmartCom both agree that the courts of England and Wales will have exclusive jurisdiction. 

17. INFORMATION OR COMPLAINTS 

17.1 If you have a question or complaint regarding the Platform, please send an e-mail to legal@smart-communications.com. You may also contact us by writing to the postal address that is found on our website. Please note that e-mail communications will not necessarily be secure; accordingly you should not include credit card information or other sensitive information in your e-mail correspondence with us. 

18. COPYRIGHT INFRINGEMENT CLAIMS 

18.1 We are committed to respecting the legal rights of copyright owners, including those who believe that material appearing on the Internet infringes their rights under either or both of English and U.S. copyright law (including the Digital Millennium Copyright Act of 1998). If you believe in good faith that materials available from the Platform infringe your copyright, you may send to SmartCom a written notice by post or e-mail, requesting that SmartCom remove such material or disable access to it. If you believe in good faith that someone has wrongly submitted to us a notice of copyright infringement involving content that you made available through the Platform, you may send to SmartCom a counter-notice. Notices and counter-notices must be sent in writing to as follows: 

(a) by mail to Legal Department, Catalyst House, 720 Centennial Court, Centennial Park, Elstree, Hertfordshire WD6 3SY; 

(b) by e-mail to legal@smart-communications.com. 

18.2 We suggest that you consult your legal advisor before sending a notice or counter-notice. It is SmartCom’s policy to terminate, in appropriate circumstances, a Platform user’s right to use the Platform if they are deemed by SmartCom to be repeat infringers. 

19. MISCELLANEOUS 

19.1 This Agreement does not, and will not be construed to, create any partnership, joint venture, employer-employee, agency or franchisor-franchisee relationship between you and SmartCom. 

19.2 If any provision of this Agreement is found to be unlawful, void or for any reason unenforceable, that provision will be deemed severable from this Agreement and will not affect the validity and enforceability of any remaining provision. 

19.3 You may not assign, transfer or sublicense any or all of your rights or obligations under this Agreement without our express prior written consent. We may assign, transfer or sublicense any or all of our rights or obligations under this Agreement without restriction. 

19.4 No waiver by either party of any breach or default under this Agreement will be deemed to be a waiver of any preceding or subsequent breach or default. 

19.5 Any heading, caption or section title contained herein is for convenience only, and in no way defines or explains any section or provision. All terms defined in the singular will have the same meanings when used in the plural, where appropriate and unless otherwise specified. 

19.6 Any use of the term “including” or variations thereof in this Agreement will be construed as if followed by the phrase “without limitation.” 

19.7 This Agreement, including any terms and conditions incorporated herein, is the entire agreement between you and SmartCom relating to the subject matter hereof, and supersedes any and all prior or contemporaneous written or oral agreements or understandings between you and SmartCom relating to such subject matter. 

19.8 Notices to you (including notices of changes to this Agreement) may be made via posting to the Platform or the associated user community page by e-mail to the most recent email address that you have provided to us (including in each case via links), or by regular mail to the most recent mailing address that you have provided to us. 

19.9 Under this Agreement, SmartCom is acting on behalf of affiliates controlled by, under common control with, or controlling SmartCom SC Limited who are not parties to the Agreement but may be entitled to enforce applicable provisions by virtue of the Contracts (Rights of Third Parties) Act 1999. 

19.10 Without limitation, a printed version of this Agreement and of any notice given in electronic form will be admissible in judicial or administrative proceedings based upon or relating to this Agreement to the same extent and subject to the same conditions as other business documents and records originally generated and maintained in printed form. 

19.11 SmartCom will not be responsible for any failure to fulfill any obligation due to any cause beyond its control. 